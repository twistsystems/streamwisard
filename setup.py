"""Setup script for stream_wisard  package."""

from setuptools import setup

setup(
    name='stream_wisard',
    use_scm_version=True,
    description="Weightless Network Wisard for Streaming Clustering",
    packages=["stream_wisard"],
    package_data={
        'stream_wisard': ['logging_config.json'],
    },
    # Requirements
    install_requires=[
        'numpy>=1.10.0',
        'setuptools_scm>=1.9.0',
        'nose>=1.3.0'
    ],
    test_suite='nose.collector',
)
