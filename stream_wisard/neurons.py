"""
RAM Neuron
"""

from __future__ import division, unicode_literals
from string import Template as T


class BaseNeuron(object):
    """
    RAM neuron implementation
    """
    def __init__(self, number_of_bits):
        """
        RAM-based neuron
        """
        self.number_of_bits = number_of_bits
        self._location = {}
        self.number_of_absorptions = 0

    def _position(self, address):
        """
        Current position

        Parameters:
        ===========
        - address: a address in bits

        Returns:
        ========
        A address decoded as a integer number
        """
        return int(''.join(['%d' % b for b in address]), 2)

    def int_to_binary_list(self, address):
        """
        Parameters:
        ===========
            - number_of_bits: number of bits
            - address: address
        Returns:
        ========
        Integer list where 1 (true) denotes the activated bits.
        """
        address_formatter = T("{0:0${number_of_bits}b}").substitute({
            'number_of_bits': self.number_of_bits
        })
        return [int(b) for b in address_formatter.format(address)]

    def output(self, address):
        """
        Neuron output for given input (address).

        Parameters:
        ===========
        - address: neuron input addresses the correct output

        Returns:
        ========
        The value stored in the RAM cell with position address
        """
        try:
            return (self._location[self._position(address)] /
                    self.number_of_absorptions)
        except KeyError:
            return 0.

    def absorb(self, address):
        """
        Absorb new entry

        Parameters:
        ===========
        - address: position to be updated
        """
        self.number_of_absorptions += 1
        i_address = self._position(address)
        try:
            self._location[i_address] += 1
        except KeyError:
            self._location[i_address] = 1
        return self.output(address)

    def bit_count(self):
        """
         The frequency each bit was activated.
        """
        freqs = []
        for addr, freq in self._location.iteritems():
            freqs.append([freq * b / self.number_of_absorptions
                          for b in self.int_to_binary_list(addr)])
        return [sum(f) for f in zip(*freqs)]

    def intersection_level(self, o_neuron):
        """
        Compare the intersection between two distinct neurons.

        Parameters:
        ==========
        - o_neuron: the neuron to be compared

        Returns:
        =======
         The percentage of common addresses activated in both RAM neurons.
        """
        # intersection / union
        return (
            len(self._location.viewkeys() & o_neuron._location.viewkeys()) /
            len(self._location.viewkeys() | o_neuron._location.viewkeys()))

    def merge(self, o_neuron):
        """
         Merge two RAM neurons

        Parameters:
        ==========
        - o_neuron: the neuron to be merged
        """
        for loc, freq in o_neuron._location.viewitems():
            try:
                self._location[loc] += freq
            except KeyError:
                self._location[loc] = freq
        self.number_of_absorptions += o_neuron.number_of_absorptions

    def __repr__(self):
        """
         Neuron representation
        """
        return 'Neuron: %s' % repr(self._location.items())


class SWNeuron(BaseNeuron):
    """
    Neuron implementation for the stream wisard network.
    """
