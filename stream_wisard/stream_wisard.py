"""
Stream Wisard implementation.
"""
from __future__ import division

from itertools import combinations
from collections import deque, defaultdict

import logging
import numpy as np

from .discriminator import SWDiscriminator
from .utils import map_input

LOGGER = logging.getLogger(__name__)


class MismatchingInput(Exception):
    """
    The input is not correct
    """
    pass


class MergingNotPossible(Exception):
    """
    Could not merge discriminators
    """
    pass


class DiscriminatorDeque(deque):
    """
    A deque implementation sorted by Discriminator compare method
    """
    def sort(self, *args, **kwargs):
        """
        Sort in-place
        """
        kwargs['reverse'] = kwargs.get('reverse', True)  # descending
        self.extend(sorted(self.pop() for x in xrange(len(self)),
                           *args, **kwargs))


class BaseClusterWISARD(object):
    """
    Base class for cluster WISARD algorithms.
    """
    discriminator_class = None

    def __init__(self, mapping, **kwargs):
        self.number_of_bits = kwargs.get('number_of_bits', 8)
        self.__discriminators = None
        self.mapping = mapping
        self.thr_merging = kwargs.get('threshold_merging', 0.75)

    @property
    def discriminators(self):
        """
        Return a list of discriminators or a empty list in case no
        discriminator is created.
        """
        if self.__discriminators is None:
            self.__discriminators = DiscriminatorDeque()
        return self.__discriminators

    @staticmethod
    def chunks(list_, size):
        """Yield successive n-sized chunks from l."""
        for i in xrange(0, len(list_), size):
            yield list_[i:i+size]

    def add_discriminator(self, discriminator):
        """
        Add a new discriminator
        """
        self.__discriminators.appendleft(discriminator)
        return True

    def simulate(self, address_list):
        """
        Run the network

        Parameters:
        ===========
        - address_list: The list of addresses to be tested

        Returns:
        ========
            The winning discriminator is returned. When no discriminator

        """
        responses = [disc.response(address_list)
                     for disc in self.discriminators]
        try:
            disc_id = np.argmax(responses)
            return self.discriminators[disc_id]
        except ValueError:
            return None

    def mental_image(self, discriminator):
        """
        Converts the drasiw content to the original space image. This will be
        considered as the "centroid" element of the discriminator.

        Parameters:
        ===========
         - discriminator: the discriminator object to be decoded.

        Returns:
        ========
            a list of integers as the mental image of a discriminator.
        """
        counter = defaultdict(int)
        drasiw = discriminator.drasiw()
        image = []
        for position in self.mapping:
            image.append(drasiw[position][counter[position]])
            counter[position] += 1
        return image

    def wisard_image(self):
        """
        Undo mapping over the drasiw's discriminators

        Returns:
        ========
            A dictionary for each
        """
        response = {}
        for discriminator in self.discriminators:
            mental_image = self.mental_image(discriminator)
            values = list(self.chunks(mental_image, self.number_of_bits))
            # round values
            round_values = []
            for value in values:
                mean_ = sum(value) / len(value)
                round_values.append([1 if x > mean_ else 0 for x in value])
            response[discriminator.id] = round_values
        return response

    def absorb(self, address_list):
        """
        The absorb functionality

        Parameters:
        ===========
        - address_list: The list of addresses to be tested
        """
        raise NotImplementedError('This method should be implemented.')

    def discriminator_similarity(self):
        """
        Compute the similarity between all discriminators
        """
        n_discriminators = len(self.discriminators)
        similarity = np.empty((n_discriminators, n_discriminators))
        similarity[:] = np.NaN
        for id_d1, id_d2 in combinations(xrange(len(self.discriminators)), 2):
            d_1 = self.discriminators[id_d1]
            d_2 = self.discriminators[id_d2]
            similarity[id_d1][id_d2] = d_1.intersection_level(d_2)
        return similarity

    def discriminator_divergence(self):
        """
         Compute the divergence between all discriminators based on their
         drasiw.
        """
        n_discriminators = len(self.discriminators)
        divergence = np.empty((n_discriminators, n_discriminators))
        divergence[:] = np.NaN
        for id_d1, id_d2 in combinations(xrange(len(self.discriminators)), 2):
            d_1 = self.discriminators[id_d1]
            d_2 = self.discriminators[id_d2]
            divergence[id_d1][id_d2] = d_1.divergence_between(d_2)
        return divergence / np.nanmax(divergence)

    def merge_discriminators(self):
        """
        Merge discriminators that are most similar to each other.

        Raises:
        =======
         MergingNotPossible if similarity is not above threshold
        """
        similarity = self.discriminator_similarity()
        sorted_array = (-similarity).argsort(axis=None, kind='mergesort')
        sorted_index = np.unravel_index(sorted_array, similarity.shape)
        discriminators = np.vstack(sorted_index).T[0]
        if similarity[discriminators[0]][discriminators[1]] < self.thr_merging:
            raise MergingNotPossible('Similarity is lower than threshold.')
        disc_1 = self.discriminators[discriminators[0]]
        disc_2 = self.discriminators[discriminators[1]]
        disc_1.merge(disc_2)
        self.discriminators.remove(disc_2)


class StreamWISARD(BaseClusterWISARD):
    """
    StreamWISARD class
    """
    discriminator_class = SWDiscriminator

    def __init__(self, mapping, **kwargs):
        """
        The StreamWISARD system is a list of discriminators. Initially there
        are no discriminators.

        When an observation is received, the list is traversed until a
        discriminator that recognizes it well enough is found. Case None is
        found, a new discriminator is created to be the data item target. At
        last, the target absorbs it and takes the front of the list.

        Parameters:
        ===========
        - minimum_similarity: The similarity threshold is a float number
        between [0, 1] (default: .5)
        - expected_absorptions: The number of entries expected to be absorbed
        in each discriminator. (default: 10)
        - number_of_bits: Number of bits of the address

        Example:
        ========

        >>> sw = StreamWISARD(mapping,
                              number_of_bits=8,
                              min_similarity=0.6,
                              max_discriminators=50,
                              expected_absorptions=10)
        >>> response = []
        >>> for irow, row in df.iterrows():
                response.append(sw.absorb(row))
        """
        super(StreamWISARD, self).__init__(mapping, **kwargs)
        self.minimum_similarity = kwargs.get('min_similarity', .5)
        self.expected_absorptions = kwargs.get('expected_absorptions', 10)
        self.max_discriminators = kwargs.get('max_discriminators', None)
        self.__discriminators = None

    def add_discriminator(self, discriminator):
        """
        Add discriminator to the system, only if the max number of
        discriminator were not reach. In this case, before deleting, the oldest
        discriminator will be deleted from the system.

        Parameters:
        ===========
        - discriminator: a Discriminator like object

        Returns:
        ========
            True if the discriminator was added to the system
        """
        self.discriminators.sort()
        if self.max_discriminators:
            if self.max_discriminators == len(self.discriminators):
                try:
                    self.merge_discriminators()
                except MergingNotPossible:
                    # Delete oldest discriminator
                    self.discriminators.pop()
                return False
        return super(StreamWISARD, self).add_discriminator(discriminator)

    def absorb(self, data_input):
        """
        The absorb functionality

        Parameters:
        ===========
        - data_input: input of data

        Returns:
        ========
            The winning discriminator is returned. When no discriminator
        """
        address_list = map_input(data_input, self.mapping)
        discriminator = self.simulate(address_list)

        if discriminator is None or not discriminator.is_activated():
            # Create new discriminator if it is the first or any one was
            # activated.
            new_discriminator = self.discriminator_class(
                len(address_list),   # number of neurons
                number_of_bits=self.number_of_bits,
                minimum_similarity=self.minimum_similarity,
                expected_absorptions=self.expected_absorptions)
            is_added = self.add_discriminator(new_discriminator)
            if is_added:
                discriminator = new_discriminator
        discriminator.absorb(address_list)
        return discriminator.id
