"""
Some utilities for streaming clustering
"""
from __future__ import division

import random
import numpy as np


def thermometer_encoder(array, bits=8, max_value=1., min_value=0.):
    """
    Convert a array of floats to the binary thermometer representation

    Parameters:
    ===========
    - array: array of floats

    Returns:
    ========
    - a thermometer representation array of binaries
    """
    bit_format = '{{0:0{}b}}'.format(bits)
    result = []
    for value in array:
        norm_value = (value - min_value) / (max_value - min_value)
        encoded_value = [
            x == '1'
            for x in bit_format.format(
                2 ** int(np.ceil(norm_value * bits)) - 1)]
        result += encoded_value
    return np.array(result, dtype=bool)


def thermometer_decoder(array, bits=8, max_value=1., min_value=0.):
    """
    Convert array of binaries to array of floats

    Parameters:
    ===========
    - array: array of floats
    """
    array_ = array.reshape([array.shape[0] / bits, bits])
    result = []
    for bit_arr in array_:
        value = int(''.join(['%d' % b for b in bit_arr]), 2) + 1
        value = np.log(value) / np.log(2 ** bits)
        value += min_value
        value *= (max_value - min_value)
        result.append(value)
    return np.array(result, dtype=float)


def generate_mapping(number_of_neurons, number_of_bits):
    """
    Generate a mapping array
    """
    mapping = range(int(number_of_neurons)) * number_of_bits
    random.shuffle(mapping)
    return mapping


def map_input(input_array, mapping):
    """
    Get address list from input_array
    """
    mapped_values = [[] for _ in set(mapping)]
    for idx, mapping_index in enumerate(mapping):
        mapped_values[mapping_index].append(input_array[idx])
    return mapped_values


def kullback_leibler(p, q):
    """
    Kullback-Leibler divergence D(P || Q) for discrete distributions

    Parameters:
    ===========
    p, q : array-like, dtype=float, shape=n
        Discrete probability distributions.
    """
    p = np.asarray(p, dtype=np.float) + 1e-10
    q = np.asarray(q, dtype=np.float) + 1e-10
    return np.sum(np.where(p != 0, p * np.log(1. * p / q), 0))
