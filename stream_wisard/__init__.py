"""
Stream Wisard implementation.

A StreamWiSARD system mantains a list of discriminators, which have the same
basic role of microclusters used in other alternatives to data streams
clustering.

Bibliography
============

1. A Weightless Neural Network-Based Approach for Stream Data Clustering,
D. Cardoso et al., 2012
2. A brief introduction to Weightless Neural Systems I. Aleksander et al., 2012

"""
