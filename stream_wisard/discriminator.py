"""
Discriminator based on RAMs addresses
"""
from __future__ import division, unicode_literals
from itertools import izip

import time
import uuid
import numpy as np

from .neurons import SWNeuron
from .utils import kullback_leibler
from copy import deepcopy


def current_milli_time():
    """
    Return:
        Integer representing the number of milliseconds
    """
    return int(time.time() * 1000)


class InvalidAddresses(Exception):
    """
    List of address is not well formed.
    """
    pass


class BaseDiscriminator(object):
    """
    Discriminator class
    """
    neuron_class = SWNeuron
    threshold = None

    def __init__(self,
                 number_of_neurons,
                 **kwargs):
        """
        A RAM-discriminator consists of a set of X one-bit word RAMs with n
        inputs and a summing device. Any such RAM-discriminator can receive a
        binary pattern of n bits as input. The RAM input lines are connected
        to the input pattern by means of a biunivocal pseudo-random mapping.

        """
        self.number_of_bits = kwargs.get('number_of_bits', 8)
        self.number_of_neurons = number_of_neurons
        self.neurons = self.init_neurons(number_of_neurons,
                                         self.number_of_bits)
        self.__id = None
        self._number_of_absorptions = 0
        self.__last_response = None
        self.merged_ = []

    @property
    def id(self):  # noqa
        """
        Discriminator id
        """
        if self.__id is None:
            self.__id = uuid.uuid4()
        return unicode(self.__id)

    def init_neurons(self, number_of_neurons, number_of_bits):
        """
        Init neurons for a RAM discriminator
        """
        raise NotImplementedError('Should create the neuron initialization.')

    def response(self, address_list):
        """
        Check whether the discriminator is suitable to input provided
        Parameters:
        ==========
        - address_list: The list of addresses to be tested

        Returns:
        ========
        A tuple containing the similarity measure and a boolean value set to
        True when the discriminator is excited.

        Raises:
        =======
        - `InvalidAddresses` when the size of the list of addresses does not
        match the number of neurons.
        """
        if len(address_list) != self.number_of_neurons:
            raise InvalidAddresses('Number of addresses differs from number of'
                                   ' neurons.')
        response = sum([
            self.neurons[idx].output(address)
            for idx, address in enumerate(address_list)
        ]) / self.number_of_neurons
        self.__last_response = response
        return response

    def __repr__(self):
        """
         Discriminator representation
        """
        return 'Discriminator: %s' % self.id

    def absorb(self, address_list):
        """
        Absorb input
        Parameters:
        ===========
        - address_list: The list of addresses to be tested
        """
        for idx, neuron in enumerate(self.neurons):
            neuron.absorb(address_list[idx])
        self._number_of_absorptions += 1

    def drasiw(self):
        """
        Returns how many times each bit was set in the addresses recorded.
        """
        return [neuron.bit_count() for neuron in self.neurons]

    def intersection_level(self, o_discriminator):
        """
        Compute the intersection level between discriminators
        This is calculated as the mean intersection level between the
        the n-th neurons of each discriminator, for every possible n.

        Parameters:
        ==========
        - o_discriminator: the discriminator that will be compared with

        Returns:
        ========
            The intersection level between the discriminators.
        """
        return np.mean([na.intersection_level(nb) for na, nb in
                        izip(self.neurons, o_discriminator.neurons)])

    def divergence_between(self, o_discriminator):
        """
        Compute the divergence between discriminators based on the
        Kullback-Leibler divergence between the drasiw images.

        Parameters:
        ==========
        - o_discriminator: the discriminator that will be compared with

        Returns:
        ========
            The computed divergence
        """
        return kullback_leibler(
            [d for n in self.drasiw() for d in n],
            [d for n in o_discriminator.drasiw() for d in n],
        )

    def is_activated(self):
        """
         Is the discriminator activated?

         Returns:
        ========
             A boolean value. False whenever the absorbed value is not greater
             than the threshold.
        """
        return self.__last_response >= self.threshold

    def merge(self, o_discriminator):
        """
        Merge two discriminators

        Parameters:
        ==========
        - o_discriminator: the discriminator that will be compared with
        """
        d_2 = deepcopy(o_discriminator)
        for n_1, n_2 in izip(self.neurons, d_2.neurons):
            n_1.merge(n_2)
        self._number_of_absorptions += d_2._number_of_absorptions
        self.merged_.append(d_2.id)

    def __cmp__(self, other_disc):
        """
         Make discriminators comparable for ordering per name
        """
        return cmp(self.id, other_disc.id)


class SWDiscriminator(BaseDiscriminator):
    """
    Discriminator implementation for the stream wisard network.
    """
    neuron_class = SWNeuron

    def __init__(self,
                 number_of_neurons,
                 **kwargs):
        """
        Overwrite default discriminator class.

        Parameters:
        ===========
        - minimum_similarity: The similarity threshold is a float number
        between [0, 1] (default: .5)
        - expected_absorptions: The number of entries expected to be absorbed
        in each discriminator. (default: 10)
        """
        super(SWDiscriminator, self).__init__(number_of_neurons, **kwargs)
        self.minimum_similarity = kwargs.get('minimum_similarity', 0.5)
        self.expected_absorptions = kwargs.get('expected_absorptions', 10)
        self.creation_time = kwargs.get('creation_time')
        if self.creation_time is None:
            self.creation_time = current_milli_time()
        self.last_update_time = self.creation_time

    def init_neurons(self, number_of_neurons, number_of_bits):
        """
        Init neurons for a RAM discriminator.

        Parameters:
        ==========
        - number of neurons: the number of neurons

        Returns:
        =======
        A list of neurons.
        """
        return [self.neuron_class(number_of_bits)
                for _ in xrange(number_of_neurons)]

    @property
    def threshold(self):
        """
        A discriminator recognizes an observation well enough to absorb it when
        its answer to a query about the data item is greater than a dynamic
        threshold.

        Returns:
            the threshold value
        """
        absorption_rate = (self._number_of_absorptions /
                           self.expected_absorptions)
        return min(self.minimum_similarity + absorption_rate, 1.)

    def absorb(self, address_list):
        """
        Overwrite default discriminator absorption. Last update time renewing
        """
        super(SWDiscriminator, self).absorb(address_list)
        self.last_update_time = current_milli_time()

    def merge(self, o_discriminator):
        """
        Merge two discriminators
        """
        super(SWDiscriminator, self).merge(o_discriminator)
        # preserve the later updating time
        self.last_update_time = max(self.last_update_time,
                                    o_discriminator.last_update_time)

    def __cmp__(self, other_disc):
        """
         Make discriminators comparable for ordering per last_update_time
        """
        return cmp((self.last_update_time, self.id),
                   (other_disc.last_update_time, other_disc.id))
