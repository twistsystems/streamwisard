"""
Test basic functionalities from discriminator class
"""
from __future__ import division
from stream_wisard.discriminator import SWDiscriminator, InvalidAddresses
from nose.tools import assert_equal
from nose.tools import assert_raises

import numpy as np


class TestDiscriminator(object):
    """
    Perform several tests on Discriminator class
    """
    @classmethod
    def setup_class(klass):
        """
        This method is run once for each class before any tests are ran
        """
        klass.number_of_bits = 4
        klass.neuron_size = 2 ** klass.number_of_bits
        klass.number_of_neurons = 3
        klass.minimum_similarity = .5
        klass.expected_absorptions = 2
        klass.inputs = [
            [[1, 0, 0, 1], [1, 1, 0, 1], [0, 0, 1, 0]],
            [[1, 0, 0, 1], [1, 1, 1, 1], [0, 0, 0, 0]],
            [[1, 0, 1, 1], [1, 1, 1, 1], [0, 0, 0, 0]]
        ]

    @classmethod
    def teardown_class(klass):
        """
        This method is run once for each class before any tests are ran
        """

    def setUp(self):
        """
        This method is run once before _each_ test method is executed
        """
        self.discriminator = SWDiscriminator(
            number_of_neurons=self.number_of_neurons,
            number_of_bits=self.number_of_bits,
            minimum_similarity=self.minimum_similarity,
            expected_absorptions=self.expected_absorptions)

    def teardown(self):
        """
        This method is run once after _each_ test method is executed
        """

    def test_dummy(self):
        """
        """
        assert(True)
    test_dummy.__doc__ = 'test'

    def test_init(self):
        """
        Test initialization
        """
        # Number of neurons
        assert_equal(len(self.discriminator.neurons), 3)
        # Initialize each neuron
        for neuron in self.discriminator.neurons:
            assert_equal(len(neuron._location), 0)

    def test_absorb(self):
        """
         Test input absorptions.

         Three inputs will be given to the same discriminator and the neurons
         will behave according to it.
        """
        discriminator = self.discriminator
        new_input = self.inputs[0]
        discriminator.absorb(new_input)
        assert_equal(discriminator.neurons[0]._location[9], 1)
        assert_equal(discriminator.neurons[1]._location[13], 1)
        assert_equal(discriminator.neurons[2]._location[2], 1)
        new_input = self.inputs[1]
        discriminator.absorb(new_input)
        assert_equal(discriminator.neurons[0]._location[9], 2)
        assert_equal(discriminator.neurons[1]._location[13], 1)
        assert_equal(discriminator.neurons[1]._location[15], 1)
        assert_equal(discriminator.neurons[2]._location[0], 1)
        assert_equal(discriminator.neurons[2]._location[2], 1)
        new_input = self.inputs[2]
        discriminator.absorb(new_input)
        assert_equal(discriminator.neurons[0]._location[9], 2)
        assert_equal(discriminator.neurons[0]._location[11], 1)
        assert_equal(discriminator.neurons[1]._location[13], 1)
        assert_equal(discriminator.neurons[1]._location[15], 2)
        assert_equal(discriminator.neurons[2]._location[0], 2)
        assert_equal(discriminator.neurons[2]._location[2], 1)

    def test_response_invalid_input(self):
        """
         Test if response raises error whenever a wrong input is provided
        """
        wrong_input = [[0, 0, 0], [0, 0, 0]]
        assert_raises(InvalidAddresses,
                      self.discriminator.response,
                      wrong_input)

    def test_response_input_unmatch(self):
        """
         Provide a response that should be reject
        """
        for new_input in self.inputs:
            self.discriminator.absorb(new_input)
        new_input = [[1, 1, 0, 0], [1, 0, 0, 0], [1, 1, 1, 0]]
        response = self.discriminator.response(new_input)
        assert_equal(response, 0)
        assert_equal(self.discriminator.is_activated(), False)
        new_input = [[1, 0, 0, 1], [1, 0, 0, 0], [1, 1, 1, 0]]
        neuron_response = [2. / 3, 0, 0]
        for idx, neuron in enumerate(self.discriminator.neurons):
            assert_equal(neuron.output(new_input[idx]), neuron_response[idx])
        response = self.discriminator.response(new_input)
        assert_equal(response, 2. / 9)

    def test_dynamic_threshold(self):
        """
         Check the dynamic threshold
        """
        dth = self.discriminator.threshold
        assert_equal(dth, 0.5)
        for new_input in self.inputs:
            self.discriminator.absorb(new_input)
        dth = self.discriminator.threshold
        assert_equal(dth, 1.)
        # 0.5 + 3 / 30
        self.discriminator.expected_absorptions = 30
        dth = self.discriminator.threshold
        assert_equal(dth, 0.6)
        # 0.5 + 3 / 100
        self.discriminator.expected_absorptions = 100
        dth = self.discriminator.threshold
        assert_equal(dth, 0.5 + 3 / 100)
