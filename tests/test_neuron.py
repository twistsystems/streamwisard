"""
Test basic functionalities from neuron class.
"""
import numpy as np
from stream_wisard.neurons import SWNeuron
from nose.tools import assert_equal


class TestNeuron(object):
    """
    Perform several tests on Neuron class
    """

    @classmethod
    def setup_class(klass):
        """
        This method is run once for each class before any tests are ran
        """
        klass.neuron_size = 2 ** 4
        klass.inputs = [[1, 0, 0, 1], [1, 1, 1, 1], [0, 0, 0, 0]]

    @classmethod
    def teardown_class(klass):
        """
        This method is run once for each class before any tests are ran
        """

    def setUp(self):
        """
        This method is run once before _each_ test method is executed
        """
        self.neuron = SWNeuron(self.neuron_size)

    def teardown(self):
        """
        This method is run once after _each_ test method is executed
        """

    def test_dummy(self):
        """
        Dummy test, only checking everything is set to go.
        """
        assert(True)

    def test_init(self):
        """
        Test neuron initialization
        It should be an array with neuron_size entries all zero.
        """
        assert_equal(len(self.neuron._location), 0)

    def test_position(self):
        """
        Test whether the class is correctly addressing the input
        """
        neuron = self.neuron
        pos_0 = neuron._position(self.inputs[0])
        pos_1 = neuron._position(self.inputs[1])
        pos_2 = neuron._position(self.inputs[2])
        assert_equal(pos_0, 9)
        assert_equal(pos_1, 15)
        assert_equal(pos_2, 0)

    def test_absorb(self):
        """
        Test neuron absorption
        """
        # First input - 3 times
        new_input = self.inputs[0]  # position 9
        output = self.neuron.absorb(new_input)
        assert_equal(output, 1)
        assert_equal(self.neuron._location[9], 1)
        assert_equal(self.neuron.number_of_absorptions, 1)
        output = self.neuron.absorb(new_input)
        assert_equal(output, 1)
        assert_equal(self.neuron._location[9], 2)
        assert_equal(self.neuron.number_of_absorptions, 2)
        output = self.neuron.absorb(new_input)
        assert_equal(output, 1)
        assert_equal(self.neuron._location[9], 3)
        assert_equal(self.neuron.number_of_absorptions, 3)
        new_input = self.inputs[1]  # position 15
        output = self.neuron.absorb(new_input)
        assert_equal(self.neuron._location[9], 3)
        assert_equal(self.neuron._location[15], 1)
        assert_equal(output, .25)
        assert_equal(self.neuron.number_of_absorptions, 4)
        new_input = self.inputs[2]  # position 1
        output = self.neuron.absorb(new_input)
        assert_equal(self.neuron._location[0], 1)
        assert_equal(self.neuron._location[9], 3)
        assert_equal(self.neuron._location[15], 1)
        assert_equal(self.neuron.number_of_absorptions, 5)
        assert_equal(output, 1. / 5)
        assert ([1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14] not in
                self.neuron._location.viewvalues())

    def test_output(self):
        """
         Test output addressing
        """
        new_input = self.inputs[0]  # position 9
        self.neuron.absorb(new_input)
        assert_equal(self.neuron.output(new_input), 1)
